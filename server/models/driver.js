const mongoose = require('mongoose');
const  { Schema } = mongoose;

const DriverSchema = new Schema({
  pilot: { type: String, required: true },
  country: { type: String, required: true },
  picture: { type: String, required: true },
  team: { type: String, required: true },
  teamLogo: { type: String, required: true}
});

module.exports = mongoose.model('Driver', DriverSchema);