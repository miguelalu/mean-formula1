const Driver = require('../models/driver');

const driverCtrl = {};

// Estas funciones me permiten hacer las consultas a la BBDD
driverCtrl.getDrivers = async (req, res) => {
 const drivers = await Driver.find();
 res.json(drivers);
};

driverCtrl.createDriver = async (req, res) => {
  const driver = new Driver({
    pilot: req.body.pilot,
    country: req.body.country,
    picture: req.body.picture,
    team: req.body.team,
    teamLogo: req.body.teamLogo
  });
  await driver.save();
  res.json({
    status: 'Driver Saved'
  });
}

driverCtrl.getDriver = async (req, res) => {
  const driver = await Driver.findById(req.params.id)
  res.json(driver);
}

driverCtrl.editDriver = async (req, res) => {
  const { id } = req.params;
  const driver = {
    pilot: req.body.pilot,
    country: req.body.country,
    picture: req.body.picture,
    team: req.body.team,
    teamLogo: req.body.teamLogo
  };
  await Driver.findByIdAndUpdate(id, {$set: driver}, {new: true});
  res.json({
    status: 'Driver Updated'
  });
}

driverCtrl.deleteDriver = async (req, res) => {
  await Driver.findByIdAndRemove(req.params.id);
  res.json({
    status: 'Driver Deleted'
  });
}

module.exports = driverCtrl;